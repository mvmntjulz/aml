import collections
import os
import os.path
import random
import sys
import time

import numpy as np
import torch
from pypokerengine.engine.hand_evaluator import HandEvaluator
from pypokerengine.utils.card_utils import _pick_unused_card, _fill_community_card, gen_cards
from pypokerengine.players import BasePokerPlayer
from torch import optim, nn


from envs import *
from network import NeuralNetwork


def set_raise_to_zero():
    global RAISES_THIS_STREET
    RAISES_THIS_STREET = 0


def increase_raise():
    global RAISES_THIS_STREET
    RAISES_THIS_STREET += 1


def estimate_win_rate(nb_simulation, nb_player, hole_card, community_card=None):
    if not community_card: community_card = []

    # Make lists of Card objects out of the list of cards
    community_card = gen_cards(community_card)
    hole_card = gen_cards(hole_card)

    # Estimate the win count by doing a Monte Carlo simulation
    win_count = sum([montecarlo_simulation(nb_player, hole_card, community_card) for _ in range(nb_simulation)])
    return 1.0 * win_count / nb_simulation


def montecarlo_simulation(nb_player, hole_card, community_card):
    # Do a Monte Carlo simulation given the current state of the game by evaluating the hands
    community_card = _fill_community_card(community_card, used_card=hole_card + community_card)
    unused_cards = _pick_unused_card((nb_player - 1) * 2, hole_card + community_card)
    opponents_hole = [unused_cards[2 * i:2 * i + 2] for i in range(nb_player - 1)]
    opponents_score = [HandEvaluator.eval_hand(hole, community_card) for hole in opponents_hole]
    my_score = HandEvaluator.eval_hand(hole_card, community_card)
    return 1 if my_score >= max(opponents_score) else 0


class MonteCarloBot(BasePokerPlayer):
    def __init__(self):
        super(MonteCarloBot, self).__init__()
        self.wins = 0
        self.losses = 0

    def declare_action(self, valid_actions, hole_card, round_state):
        # Estimate the win rate
        win_rate = estimate_win_rate(MC_ITERATIONS, self.num_players, hole_card, round_state['community_card'])

        # Check whether it is possible to call
        can_call = len([item for item in valid_actions if item['action'] == 'call']) > 0
        if can_call:
            # If so, compute the amount that needs to be called
            call_amount = [item for item in valid_actions if item['action'] == 'call'][0]['amount']
        else:
            call_amount = 0

        amount = None

        if win_rate > 0.5:
            raise_amount_options = [item for item in valid_actions if item['action'] == 'raise'][0]['amount']
            global RAISES_THIS_STREET
            if win_rate > 0.85 and RAISES_THIS_STREET < MAX_RAISES:
                increase_raise()
                action = 'raise'
                amount = min(raise_amount_options['min']*RAISE_LIMIT, raise_amount_options['max'])
            elif win_rate > 0.75 and RAISES_THIS_STREET < MAX_RAISES:
                increase_raise()
                action = 'raise'
                amount = min(raise_amount_options['min']*RAISE_LIMIT, raise_amount_options['max'])
            else:
                action = 'call'
        else:
            action = 'fold' if can_call and call_amount == 0 else 'fold'

        # Set the amount
        if amount is None:
            items = [item for item in valid_actions if item['action'] == action]
            amount = items[0]['amount']

        return action, amount

    def receive_game_start_message(self, game_info):
        self.num_players = game_info['player_num']

    def receive_round_start_message(self, round_count, hole_card, seats):
        pass

    def receive_street_start_message(self, street, round_state):
        set_raise_to_zero()

    def receive_game_update_message(self, action, round_state):
        pass

    def receive_round_result_message(self, winners, hand_info, round_state):
        is_winner = self.uuid in [item['uuid'] for item in winners]
        self.wins += int(is_winner)
        self.losses += int(not is_winner)


class NNTrainBot(BasePokerPlayer):
    STREETS = ["preflop", "flop", "turn", "river", "showdown"]

    def __init__(self):
        super().__init__()
        if torch.cuda.is_available() and CUDA:
            torch.set_default_tensor_type('torch.cuda.FloatTensor')

        self.network = NeuralNetwork()
        if torch.cuda.is_available() and CUDA:
            self.network = self.network.cuda()

        self.optimizer = optim.Adam(self.network.parameters(), lr=1e-6)
        self.criterion = nn.MSELoss()
        self.epsilon = self.network.initial_epsilon
        self.hole_card = None

        self.replay_memory = []
        self.state = None
        self.action = None
        self.stack = 0

        self.action_hist = {
            "fold": 0,
            "raise": 0,
            "call": 0
        }

        if not os.path.exists("experiments"):
            os.makedirs("experiments")
        if not os.path.exists(os.path.join("experiments", EXPERIMENT_NAME)):
            os.makedirs(os.path.join("experiments", EXPERIMENT_NAME))
        else:
            print("Experiment with this name already exists!")
            sys.exit(0)

        self.time = time.time()

    def declare_action(self, valid_actions, hole_card, round_state):
        # previous state is available
        if self.state is not None:
            state_1 = self.get_state(round_state, hole_card)
            reward = 0

            self.q_computation(round_state, reward, state_1)

        self.state = self.get_state(round_state, hole_card)
        if torch.cuda.is_available() and CUDA:
            self.state = self.state.cuda()

        output = self.network(self.state)
        self.action = torch.zeros([self.network.number_of_actions], dtype=torch.float32)

        if torch.cuda.is_available() and CUDA:  # put on GPU if CUDA is available
            self.action = self.action.cuda()

        # exploration vs. exploitation
        action_index = [torch.randint(self.network.number_of_actions, torch.Size([]), dtype=torch.int)
                        if random.random() <= self.epsilon
                        else torch.argmax(output)][0]

        if torch.cuda.is_available() and CUDA:  # put on GPU if CUDA is available
            action_index = action_index.cuda()

        self.action[action_index] = 1
        next_action = ["fold", "call", "raise"][(self.action == 1).nonzero()]

        self.action_hist[next_action] += 1

        amount = self.get_amount(next_action, valid_actions)

        return next_action, amount

    def q_computation(self, round_state, reward, state_1):
        # save new replay to list
        self.replay_memory.append((self.state, self.action, reward, state_1, False))
        if len(self.replay_memory) > self.network.replay_memory_size:
            self.replay_memory.pop(0)

        # annealing
        self.epsilon -= self.network.epsilon_decrease if self.network.final_epsilon else 0

        # sample random minibatch
        minibatch = random.sample(self.replay_memory, min(len(self.replay_memory), self.network.minibatch_size))

        # unpack minibatch
        state_batch = torch.stack(tuple(d[0] for d in minibatch))
        action_batch = torch.stack(tuple(d[1] for d in minibatch))
        reward_batch = torch.stack(tuple(torch.from_numpy(np.asarray(d[2], dtype=np.float32)) for d in minibatch),
                                   dim=0)
        state_1_batch = torch.stack(tuple(d[3] for d in minibatch))

        if torch.cuda.is_available() and CUDA:  # put on GPU if CUDA is available
            state_batch = state_batch.cuda()
            action_batch = action_batch.cuda()
            reward_batch = reward_batch.cuda()
            state_1_batch = state_1_batch.cuda()

        # network output for follow states
        output_1_batch = self.network(state_1_batch)

        if torch.cuda.is_available() and CUDA:
            output_1_batch = output_1_batch.cuda()

        y_batch = torch.stack(tuple(reward_batch[i] if minibatch[i][4]
                                    else reward_batch[i] + self.network.gamma * torch.max(output_1_batch[i])
                                    for i in range(len(minibatch))))
        q_value = torch.sum(self.network(state_batch) * action_batch, dim=1)

        # reset gradients
        self.optimizer.zero_grad()
        # compute loss and do back propagation
        y_batch = y_batch.detach()
        loss = self.criterion(q_value, y_batch)
        loss.backward()
        self.optimizer.step()

        return loss

    def get_amount(self, next_action, valid_actions):
        global RAISES_THIS_STREET
        if next_action == "fold":
            amount = 0
        elif next_action == "raise" and RAISES_THIS_STREET <= MAX_RAISES:
            increase_raise()
            amount = min(valid_actions[2]["amount"]["min"] * RAISE_LIMIT, valid_actions[2]["amount"]["max"])
        else:
            amount = valid_actions[1]["amount"]
        return amount

    def receive_game_start_message(self, game_info):
        self.stack = [s["stack"] for s in game_info["seats"] if s["uuid"] == self.uuid][0]

    def receive_round_start_message(self, round_count, hole_card, seats):
        self.save_round_result(round_count, seats)
        self.state = None
        self.hole_card = hole_card

    def save_round_result(self, round_count, seats):
        if not round_count % PRINT_EVERY:
            stack_string = ""
            for s in [(s["name"], s["stack"]) for s in seats]:
                stack_string += "," + s[0]
                stack_string += "," + str(s[1])

            line = "{},{},{},{}{}".format(
                round_count,
                self.action_hist["fold"],
                self.action_hist["call"],
                self.action_hist["raise"],
                stack_string)

            print(line)
            with open(os.path.join("experiments", EXPERIMENT_NAME if TRAIN else EXPERIMENT_NAME + "_test", "round_results.txt"), "a") as f:
                f.write(line + "\n")

    def receive_street_start_message(self, street, round_state):
        set_raise_to_zero()

    def receive_game_update_message(self, action, round_state):
        pass

    def receive_round_result_message(self, winners, hand_info, round_state):
        if self.state is not None:
            state_1 = self.get_state(round_state, self.hole_card)

            is_showdown = [item for item in round_state["seats"] if item["state"] == "participating"]
            player_in_showdown = len([w for w in is_showdown if w["uuid"] == self.uuid])
            is_showdown = len(is_showdown)

            current_stack = [s["stack"] for s in round_state["seats"] if s["uuid"] == self.uuid][0]
            if is_showdown >= 2 and player_in_showdown or bool(self.action[0]):
                reward = current_stack - self.stack
            else:
                reward = 0

            self.stack = current_stack

            loss = self.q_computation(round_state, reward, state_1)
            if not round_state["round_count"] % PRINT_EVERY:
                if round_state["round_count"] == 1000:
                    current_time = time.time()
                    time_delta = (current_time - self.time)
                    print("Time-Delta: {}".format(time_delta))
                    print("Predicted Exp. Time in m ({} iterations): {}".format(MAX_ROUNDS, time_delta * MAX_ROUNDS / 60000.0))
                print("Loss: {}".format(loss))
                print("====================================")

        if not round_state["round_count"] % SAVE_EVERY:
            torch.save(self.network.state_dict(), os.path.join(
                "experiments",
                EXPERIMENT_NAME,
                "model_{}".format(round_state["round_count"])))

            print("Saved model (model_{})".format(round_state["round_count"]))

    def get_state(self, round_state, hole_card):
        t = round_state["street"]
        p = round_state["big_blind_pos"]

        x = self.get_raises_this_stage(round_state, t) if t != "showdown" else 0

        community_cards = gen_cards(round_state["community_card"])
        hole_cards = gen_cards(hole_card)

        high = HandEvaluator.gen_hand_rank_info(hole_cards, community_cards)["hand"]["high"]

        x1, x2, x3, x4, h1, h2, s = -1, -1, -1, -1, -1, -1, -1
        if not t == "preflop":
            x1 = self.get_amount_of_doubles(community_cards, hole_cards)
            x2 = self.get_number_of_cards_lower_or_equal_to_high_card(community_cards, high)
            x3 = self.get_flush_feature(community_cards, hole_cards)
            x4 = self.get_straight_feature(community_cards, hole_cards, round_state)
        else:
            hc = HandEvaluator.gen_hand_rank_info(hole_cards, community_cards)["hole"]
            h1, h2 = hc["high"], hc["low"]

            s = hole_card[0][0] == hole_card[1][0]

        f = [self.STREETS.index(t), p, x, x1, x2, x3, x4, h1, h2, s]
        fnpy = np.asarray(f, dtype=np.float32) + 1.0
        return torch.from_numpy(fnpy)

    def get_raises_this_stage(self, round_state, t):
        x = 0
        info = round_state["action_histories"][t]
        for action in info:
            if action["action"] == "RAISE":
                x += 1
        return x

    def get_amount_of_doubles(self, community_cards, hole_cards):
        x1 = 0
        for c in community_cards:
            for h in hole_cards:
                if c.rank == h.rank:
                    x1 += 1
                    break
        return x1

    def get_number_of_cards_lower_or_equal_to_high_card(self, community_cards, high):
        x2 = 0
        for c in community_cards:
            if c.rank <= high:
                x2 += 1
        return x2

    def get_flush_feature(self, community_cards, hole_cards):
        suits = [c.suit for c in community_cards]
        flash_candidates = [item for item, count in collections.Counter(suits).items() if count == 3]
        double_candidates = [item for item, count in collections.Counter(suits).items() if count == 2]
        hole_matches = 0

        x3 = 0
        # if there is a flush_draw...
        if flash_candidates:
            for c in hole_cards:
                # count flush cards in hole cards
                if c.suit == flash_candidates[0]:
                    hole_matches +=1
            # ...and the player holds one flush card return a value of 1
            if hole_matches == 1:
                x3 = 1
            # ...and the player holds two flush card return a value of 2
            elif hole_matches == 2:
                x3 = 2

        if not x3 and double_candidates:
            # if there are two cards of same suit on the board...
            for c in hole_cards:
                if c.suit == double_candidates[0]:
                    hole_matches +=1
            # ...and the player also holds two cards of that suit in hand return 3
            if hole_matches == 2:
                x3 = 3
        return x3

    def get_straight_feature(self, community_cards, hole_cards, round_state):
        is_straight = "STRAIGHT" == HandEvaluator.gen_hand_rank_info(hole_cards, community_cards)["hand"]["strength"]
        if is_straight:
            # if player already holds a straight return 2
            x4 = 2
        else:
            ranks = [c.rank for c in hole_cards + community_cards]
            if self.is_ace_in_ranks(ranks):
                ranks.append(1)

            uniques = sorted(set(ranks))
            if len(uniques) < 4:
                # if there is no straight draw return 0
                x4 = 0
            else:
                possible_straights = []

                for i in range(len(uniques)-3):
                    fours = uniques[i:i+4]
                    if fours[-1] - fours[0] <= 4:
                        possible_straights.append(fours)
                if possible_straights:
                    # if there is a straight draw and we expect another community card return 1 else 0
                    x4 = 1 if round_state["street"] != "river" else 0
                else:
                    x4 = 0
        return x4

    def is_ace_in_ranks(self, ranks):
        return 14 in ranks


class NNTestBot(NNTrainBot):
    def __init__(self):
        if torch.cuda.is_available() and CUDA:
            torch.set_default_tensor_type('torch.cuda.FloatTensor')

        self.network = NeuralNetwork()
        if torch.cuda.is_available() and CUDA:
            self.network = self.network.cuda()

        self.network.load_state_dict(torch.load(os.path.join("experiments", MODEL_NAME, f'model_{MODEL_NBR}')))

        self.action_hist = {
            "fold": 0,
            "raise": 0,
            "call": 0
        }

        if not os.path.exists("experiments"):
            os.makedirs("experiments")
        if not os.path.exists(os.path.join("experiments", EXPERIMENT_NAME + "_test")):
            os.makedirs(os.path.join("experiments", EXPERIMENT_NAME + "_test"))
        else:
            print("Experiment with this name already exists!")
            sys.exit(0)

    def declare_action(self, valid_actions, hole_card, round_state):
        output = self.network(self.get_state(round_state, hole_card))
        action_index = [torch.argmax(output)][0]

        if torch.cuda.is_available() and CUDA:  # put on GPU if CUDA is available
            action_index = action_index.cuda()

        action = torch.zeros([self.network.number_of_actions], dtype=torch.float32)
        action[action_index] = 1
        next_action = ["fold", "call", "raise"][(action == 1).nonzero()]
        self.action_hist[next_action] += 1

        return next_action, self.get_amount(next_action, valid_actions)

    def receive_round_result_message(self, winners, hand_info, round_state):
        # Necessary to avoid backward-steps from training (super-class)
        pass


class RndBot(BasePokerPlayer):
    def declare_action(self, valid_actions, hole_card, round_state):
        if random.random() < 0.06:
            return "fold", 0
        rnd_action = random.choice(valid_actions[1:])
        action = rnd_action["action"]
        amount = 0
        global RAISES_THIS_STREET
        if action == "raise" and RAISES_THIS_STREET < MAX_RAISES:
            increase_raise()
            amount = min(rnd_action["amount"]["min"]*RAISE_LIMIT, rnd_action["amount"]["max"])
        elif action == "raise" or action == "call":
            action = "call"
            amount = valid_actions[1]["amount"]

        return action, amount

    def receive_game_start_message(self, game_info):
        pass

    def receive_round_start_message(self, round_count, hole_card, seats):
        pass

    def receive_street_start_message(self, street, round_state):
        set_raise_to_zero()

    def receive_game_update_message(self, action, round_state):
        pass

    def receive_round_result_message(self, winners, hand_info, round_state):
        pass
