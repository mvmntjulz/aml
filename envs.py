DEBUG = False
TRAIN = True
DRAW = False
DRAW_SAVE = False
CUDA = False

EXPERIMENT_NAME = "nn_rnd{}".format("" if TRAIN else "_test")
MODEL_NAME = "nn_rnd"
MODEL_NBR = 150000  # rounds the model learned
PRINT_EVERY = 1000  # print the result every x rounds
SAVE_EVERY = 10000  # save the model every x rounds

# Monte-Cralo-Bot settings
MC_ITERATIONS = 100  # simulation runs for the MonteCarlo-Bot

# game settings:
SMALL_BLIND = 5  # minimum bet every round
MAX_ROUNDS = int(2e5)  # maximum rounds to play
INIT_STACK = int(1e7) # initial stack size
MAX_RAISES = 2  # times, players can raise in each street
RAISE_LIMIT = 2  # factors the amount of a raise (RAISE_LIMIT * minimal_raise)

# network settings:
NUMBER_OF_ACTIONS = 3  # fold, call, raise
GAMMA = 1  # fixed discount factor for RL
INITIAL_EPSILON = 0.2  # starting value for the greedy epsilon approach
FINAL_EPSILON = 0.0001  # final epsilon
EPSILON_DECREASE = 1e-6  # decreasing rate of the epsilon
REPLAY_MEMORY_SIZE = 30000  # pool of (s,a,r,s',t)-tuples to choose from during training
MINIBATCH_SIZE = 100  # size of tuple for each minibatch
