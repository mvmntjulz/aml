Reinforcement Learning with Neural Networks for Texas Holdem Poker
==================================================================

Authors: Maximilian Klingmann, Julien Stern

Submission Date: 07/10/18

---

agents.py - Implementation of Neural Network Bot for training (NN-TrainBot) and testing (NN-TestBot) and baseline opponents (MonteCarlo-Bot & Random-Bot)

envs.py - Settings, Hyper-parameters and parameters for the Neural Network

main.py - Main-Function, Plotting and Choice of Players

network.py - Neural Network used by the NN-Bots.


