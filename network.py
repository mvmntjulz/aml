from torch import nn
from envs import *


class NeuralNetwork(nn.Module):
    def __init__(self):
        super(NeuralNetwork, self).__init__()

        self.number_of_actions = NUMBER_OF_ACTIONS
        self.gamma = GAMMA
        self.final_epsilon = FINAL_EPSILON
        self.initial_epsilon = INITIAL_EPSILON
        self.epsilon_decrease = EPSILON_DECREASE
        self.replay_memory_size = REPLAY_MEMORY_SIZE
        self.minibatch_size = MINIBATCH_SIZE

        self.fc1 = nn.Linear(10, 21)
        self.relu1 = nn.ReLU(inplace=True)
        self.fc2 = nn.Linear(21, 42)
        self.relu2 = nn.ReLU(inplace=True)
        self.fc3 = nn.Linear(42, 21)
        self.relu3 = nn.ReLU(inplace=True)
        self.fc4 = nn.Linear(21, self.number_of_actions)

    def forward(self, x):
        out = self.fc1(x)
        out = self.relu1(out)
        out = self.fc2(out)
        out = self.relu2(out)
        out = self.fc3(out)
        out = self.relu3(out)
        out = self.fc4(out)

        return out
