from pypokerengine.api.game import setup_config, start_poker

import matplotlib.pyplot as plt
from agents import *
from envs import *


def draw_stacks():
    with open(os.path.join("experiments", EXPERIMENT_NAME, "round_results.txt"), "r") as f:
        lines = np.asarray([(x.strip()).split(",") for x in f.readlines()])
        player_colors = ['-r', '-b', '-g', '-y', '-k']
        numpy_stack = np.asarray([list(a) for a in zip(lines[:, 5], lines[:, 7])], dtype=np.uint32)
        rounds = list(np.linspace(1000, numpy_stack.shape[0]*1000, numpy_stack.shape[0], dtype=np.uint32))

        fig = plt.figure()
        for i in range(numpy_stack.shape[1]):
            plt.plot(rounds, numpy_stack[:, i], player_colors[i], label=lines[0][4] if i == 0 else lines[0][6])

        plt.legend()
        plt.xlabel("Rounds")
        plt.ylabel("Stack")
        plt.ticklabel_format(style='sci', axis='both', scilimits=(0, 0))
        fig.savefig(os.path.join("experiments", EXPERIMENT_NAME, "stacks")) if DRAW_SAVE else plt.show()


def draw_nn_actions():
    with open(os.path.join("experiments", EXPERIMENT_NAME, "round_results.txt"), "r") as f:
        lines = np.asarray([(x.strip()).split(",") for x in f.readlines()])
        action_colors = ['-g', '-b', '-r']
        numpy_stack = np.asarray([list(a) for a in zip(lines[:, 1], lines[:, 2], lines[:, 3])], dtype=np.uint32)
        rounds = list(np.linspace(1000, numpy_stack.shape[0]*1000, numpy_stack.shape[0], dtype=np.uint32))

        fig = plt.figure()
        for i in range(numpy_stack.shape[1]):
            plt.plot(rounds, numpy_stack[:, i], action_colors[i], label="fold" if i == 0 else ("call" if i == 1 else "raise"))

        plt.legend()
        plt.xlabel("Rounds")
        plt.ylabel("Number of Action")
        plt.ticklabel_format(style='sci', axis='both', scilimits=(0, 0))
        #plt.xlim([0, 30000])
        #plt.ylim([0, 80000])
        fig.savefig(os.path.join("experiments", EXPERIMENT_NAME, "actions")) if DRAW_SAVE else plt.show()


def setup():
    config = setup_config(max_round=MAX_ROUNDS, initial_stack=INIT_STACK, small_blind_amount=SMALL_BLIND)
    # config.register_player(name="MC", algorithm=MonteCarloBot())
    config.register_player(name="Random1", algorithm=RndBot())
    config.register_player(name="NN", algorithm=(NNTrainBot() if TRAIN else NNTestBot()))
    return config


if __name__ == "__main__":
    if DRAW:
        draw_stacks()
        draw_nn_actions()
    else:
        game_result = start_poker(setup(), verbose=DEBUG)
